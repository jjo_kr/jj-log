"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 로그 작성 전반을 다루는 유틸리티 클래스
 *
 * 로그 내용에 `%LOG_COLOR%` 꼴의 문구를 붙여 서식을 지정할 수 있다.
 * 사용 가능한 서식의 종류는 열거형 {@link LogColor}에서 명시하고 있다.
 * @example
 * // 파란 바탕에 밝은 초록 글씨로 A를 출력하고, 기본 서식으로 B를 출력
 * TinyLogger.log("%B_BLUE%%BRIGHT%%F_GREEN%A%NORMAL%B");
 */
class TinyLogger {
    static print(type, data) {
        let now = new Date();
        let typeText;
        switch (type) {
            case LogType.LOG:
                typeText = "%BRIGHT%LOG%NORMAL%";
                break;
            case LogType.INFO:
                typeText = "%B_BLUE%INF%NORMAL%";
                break;
            case LogType.SUCCESS:
                typeText = "%B_GREEN%%F_BLACK%OK!%NORMAL%";
                break;
            case LogType.WARN:
                typeText = "%B_YELLOW%%F_BLACK%WAR%NORMAL%";
                break;
            case LogType.ERROR:
                typeText = "%B_RED%%BRIGHT%ERR%NORMAL%";
                break;
            default: throw Error("Unhandled log type: " + type);
        }
        TinyLogger.printPure(`%BRIGHT%[${now.toLocaleString()}]%NORMAL% ${typeText} ${data}`, TinyLogger.printInWebkit);
    }
    static printPure(message, inWebkit = false) {
        const now = new Date();
        let time, timeR;
        if (inWebkit) {
            TinyLogger.printWebkit(message);
        }
        else {
            console.log(message.replace(TinyLogger.ESCAPE_MATCHER, (v, p1) => {
                if (LogColor.hasOwnProperty(p1))
                    return `\x1b[${LogColor[p1]}m`;
                return v;
            }));
        }
    }
    static prettyListing(array) {
        let factories;
        let length = array.length;
        if (length > 1) {
            factories = [(v, i) => v];
            factories[length - 1] = (v, i) => `%BRIGHT%└ #${i}:%NORMAL% ${v}`;
            return array.map((v, i) => {
                return factories.hasOwnProperty(String(i)) ? factories[i](v, i) : `%BRIGHT%├ #${i}:%NORMAL% ${v}`;
            }).join('\n');
        }
        return String(array[0]);
    }
    /**
     * 웹킷 서식 메시지를 출력한다.
     *
     * @param message 메시지
     */
    static printWebkit(message) {
        let chunk;
        let recentChunkEndIndex = 0;
        let styleQueue = [];
        let text = [];
        let args = [];
        let textQueue = [];
        while (chunk = TinyLogger.ESCAPE_MATCHER.exec(message)) {
            const t = message.slice(recentChunkEndIndex, chunk.index);
            if (t.length > 0 && LogColor[chunk[1]] !== LogColor.NORMAL) {
                textQueue.push("%c" + t);
                args.push("");
            }
            switch (LogColor[chunk[1]]) {
                case LogColor.B_BLACK:
                    styleQueue.push("background: black;");
                    break;
                case LogColor.B_BLUE:
                    styleQueue.push("background: blue;");
                    break;
                case LogColor.B_CYAN:
                    styleQueue.push("background: cyan;");
                    break;
                case LogColor.B_GREEN:
                    styleQueue.push("background: green;");
                    break;
                case LogColor.B_MAGENTA:
                    styleQueue.push("background: magenta;");
                    break;
                case LogColor.B_RED:
                    styleQueue.push("background: red;");
                    break;
                case LogColor.B_WHITE:
                    styleQueue.push("background: white;");
                    break;
                case LogColor.B_YELLOW:
                    styleQueue.push("background: yellow;");
                    break;
                case LogColor.BRIGHT:
                    styleQueue.push("font-weight: bold;");
                    break;
                case LogColor.DIM:
                    styleQueue.push("font-style: italic;");
                    break;
                case LogColor.F_BLUE:
                    styleQueue.push("color: blue;");
                    break;
                case LogColor.F_CYAN:
                    styleQueue.push("color: cyan;");
                    break;
                case LogColor.F_GREEN:
                    styleQueue.push("color: green;");
                    break;
                case LogColor.F_MAGENTA:
                    styleQueue.push("color: magenta;");
                    break;
                case LogColor.F_RED:
                    styleQueue.push("color: red;");
                    break;
                case LogColor.F_WHITE:
                    styleQueue.push("color: white;");
                    break;
                case LogColor.F_YELLOW:
                    styleQueue.push("color: yellow;");
                    break;
                case LogColor.UNDERSCORE:
                    styleQueue.push("text-decoration: underline;");
                    break;
                case LogColor.NORMAL:
                    text.push(textQueue.join('') + "%c" + t);
                    textQueue = [];
                    args.push(styleQueue.join(' '));
                    styleQueue = [];
                    break;
            }
            recentChunkEndIndex = chunk.index + chunk[0].length;
        }
        if (recentChunkEndIndex < message.length) {
            text.push("%c" + message.slice(recentChunkEndIndex));
            args.push("");
        }
        console.log(text.join(''), ...args);
    }
    /**
     * 별도 처리를 하지 않고 인자의 내용을 그대로 출력한다.
     *
     * @param message 메시지
     */
    static pure(message) {
        TinyLogger.printPure(message);
    }
    /**
     * 일반 로그를 출력한다.
     *
     * @param args 추가 정보
     */
    static log(...args) {
        TinyLogger.print(LogType.LOG, TinyLogger.prettyListing(args));
    }
    /**
     * 안내 로그를 출력한다.
     *
     * @param args 추가 정보
     */
    static info(...args) {
        TinyLogger.print(LogType.INFO, TinyLogger.prettyListing(args));
    }
    /**
     * 성공 로그를 출력한다.
     *
     * @param args 추가 정보
     */
    static success(...args) {
        TinyLogger.print(LogType.SUCCESS, TinyLogger.prettyListing(args));
    }
    /**
     * 경고 로그를 출력한다.
     *
     * @param args 추가 정보
     */
    static warn(...args) {
        TinyLogger.print(LogType.WARN, TinyLogger.prettyListing(args));
    }
    /**
     * 오류 로그를 출력한다.
     *
     * 이 메소드가 호출된다 해도 작업이 중단되지는 않는다.
     * @param head 오류 유형
     * @param args 추가 정보
     */
    static error(head, ...args) {
        let message;
        TinyLogger.print(LogType.ERROR, message = TinyLogger.prettyListing([
            `%BRIGHT%${head}%NORMAL%`,
            ...args
        ]));
        // throw Error(message);
    }
}
/**
 * 웹킷 콘솔 출력 양식 따르기 여부
 */
TinyLogger.printInWebkit = false;
/**
 * 탈출 문자 정규 표현식
 */
TinyLogger.ESCAPE_MATCHER = /%([A-Z_]+)%/g;
exports.default = TinyLogger;
var LogType;
(function (LogType) {
    LogType[LogType["LOG"] = 0] = "LOG";
    LogType[LogType["INFO"] = 1] = "INFO";
    LogType[LogType["SUCCESS"] = 2] = "SUCCESS";
    LogType[LogType["WARN"] = 3] = "WARN";
    LogType[LogType["ERROR"] = 4] = "ERROR";
})(LogType || (LogType = {}));
var LogColor;
(function (LogColor) {
    LogColor[LogColor["NORMAL"] = 0] = "NORMAL";
    LogColor[LogColor["BRIGHT"] = 1] = "BRIGHT";
    LogColor[LogColor["DIM"] = 2] = "DIM";
    LogColor[LogColor["UNDERSCORE"] = 4] = "UNDERSCORE";
    LogColor[LogColor["F_BLACK"] = 30] = "F_BLACK";
    LogColor[LogColor["F_RED"] = 31] = "F_RED";
    LogColor[LogColor["F_GREEN"] = 32] = "F_GREEN";
    LogColor[LogColor["F_YELLOW"] = 33] = "F_YELLOW";
    LogColor[LogColor["F_BLUE"] = 34] = "F_BLUE";
    LogColor[LogColor["F_MAGENTA"] = 35] = "F_MAGENTA";
    LogColor[LogColor["F_CYAN"] = 36] = "F_CYAN";
    LogColor[LogColor["F_WHITE"] = 37] = "F_WHITE";
    LogColor[LogColor["B_BLACK"] = 40] = "B_BLACK";
    LogColor[LogColor["B_RED"] = 41] = "B_RED";
    LogColor[LogColor["B_GREEN"] = 42] = "B_GREEN";
    LogColor[LogColor["B_YELLOW"] = 43] = "B_YELLOW";
    LogColor[LogColor["B_BLUE"] = 44] = "B_BLUE";
    LogColor[LogColor["B_MAGENTA"] = 45] = "B_MAGENTA";
    LogColor[LogColor["B_CYAN"] = 46] = "B_CYAN";
    LogColor[LogColor["B_WHITE"] = 47] = "B_WHITE";
})(LogColor || (LogColor = {}));
//# sourceMappingURL=index.js.map