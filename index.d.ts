declare module 'jj-log/Tiny' {
	/**
	 * 로그 작성 전반을 다루는 유틸리티 클래스
	 *
	 * 로그 내용에 `%LOG_COLOR%` 꼴의 문구를 붙여 서식을 지정할 수 있다.
	 * 사용 가능한 서식의 종류는 열거형 {@link LogColor}에서 명시하고 있다.
	 * @example
	 * // 파란 바탕에 밝은 초록 글씨로 A를 출력하고, 기본 서식으로 B를 출력
	 * TinyLogger.log("%B_BLUE%%BRIGHT%%F_GREEN%A%NORMAL%B");
	 */
	export default class TinyLogger {
	    /**
	     * 웹킷 콘솔 출력 양식 따르기 여부
	     */
	    static printInWebkit: boolean;
	    /**
	     * 탈출 문자 정규 표현식
	     */
	    static ESCAPE_MATCHER: RegExp;
	    private static print(type, data);
	    private static printPure(message, inWebkit?);
	    private static prettyListing(array);
	    /**
	     * 웹킷 서식 메시지를 출력한다.
	     *
	     * @param message 메시지
	     */
	    static printWebkit(message: string): void;
	    /**
	     * 별도 처리를 하지 않고 인자의 내용을 그대로 출력한다.
	     *
	     * @param message 메시지
	     */
	    static pure(message: string): void;
	    /**
	     * 일반 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static log(...args: any[]): void;
	    /**
	     * 안내 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static info(...args: any[]): void;
	    /**
	     * 성공 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static success(...args: any[]): void;
	    /**
	     * 경고 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static warn(...args: any[]): void;
	    /**
	     * 오류 로그를 출력한다.
	     *
	     * 이 메소드가 호출된다 해도 작업이 중단되지는 않는다.
	     * @param head 오류 유형
	     * @param args 추가 정보
	     */
	    static error(head: string, ...args: any[]): void;
	}

}
declare module 'jj-log' {
	/**
	 * 로그 작성 전반을 다루는 유틸리티 클래스
	 *
	 * 로그 내용에 `%LOG_COLOR%` 꼴의 문구를 붙여 서식을 지정할 수 있다.
	 * 사용 가능한 서식의 종류는 열거형 {@link LogColor}에서 명시하고 있다.
	 * @example
	 * // 파란 바탕에 밝은 초록 글씨로 A를 출력하고, 기본 서식으로 B를 출력
	 * Logger.log("%B_BLUE%%BRIGHT%%F_GREEN%A%NORMAL%B");
	 */
	export default class Logger {
	    /**
	     * 웹킷 콘솔 출력 양식 따르기 여부
	     */
	    static printInWebkit: boolean;
	    private static destFile;
	    private static destPathPrefix;
	    private static destPathSuffix;
	    private static _prevDestSwitched;
	    private static destSwitchInterval;
	    private static print(type, data);
	    private static printPure(message, now);
	    private static prettyListing(array);
	    private static setDestFile(name);
	    /**
	     * 로그 파일 이름의 접사를 설정한다.
	     *
	     * 로그 파일이 생성될 때의 시각에 접두사(prefix)와 접미사(suffix)를 덧붙여 이름을 결정한다.
	     * @param pathPrefix 로그 파일 이름의 접두사
	     * @param pathSuffix 로그 파일 이름의 접미사. 확장자를 포함한다.
	     */
	    static setDestFileAffix(pathPrefix: string, pathSuffix?: string): void;
	    /**
	     * 로그 파일 경로를 바꾸는 간격을 설정한다.
	     *
	     * 하루마다 로그 파일 경로를 바꾸려면 인자로 `86400000`을 전달한다.
	     * @param interval 변경 간격(밀리초). 0인 경우 로그 파일 경로를 변경하지 않는다.
	     */
	    static setDestSwitchInterval(interval: number): void;
	    /**
	     * 별도 처리를 하지 않고 인자의 내용을 그대로 출력한다.
	     *
	     * @param message 메시지
	     * @param timestamp 타임스탬프
	     */
	    static pure(message: string, timestamp?: Date): void;
	    /**
	     * 일반 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static log(...args: any[]): void;
	    /**
	     * 안내 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static info(...args: any[]): void;
	    /**
	     * 성공 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static success(...args: any[]): void;
	    /**
	     * 경고 로그를 출력한다.
	     *
	     * @param args 추가 정보
	     */
	    static warn(...args: any[]): void;
	    /**
	     * 오류 로그를 출력한다.
	     *
	     * 이 메소드가 호출된다 해도 작업이 중단되지는 않는다.
	     * @param head 오류 유형
	     * @param args 추가 정보
	     */
	    static error(head: string, ...args: any[]): void;
	}

}
