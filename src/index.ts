import Cluster = require('cluster');
import FS = require('fs');
import Format from "jj-format";
import TinyLogger from "./Tiny";

/**
 * 로그 작성 전반을 다루는 유틸리티 클래스
 * 
 * 로그 내용에 `%LOG_COLOR%` 꼴의 문구를 붙여 서식을 지정할 수 있다.
 * 사용 가능한 서식의 종류는 열거형 {@link LogColor}에서 명시하고 있다.
 * @example
 * // 파란 바탕에 밝은 초록 글씨로 A를 출력하고, 기본 서식으로 B를 출력
 * Logger.log("%B_BLUE%%BRIGHT%%F_GREEN%A%NORMAL%B");
 */
export default class Logger{
	/**
	 * 웹킷 콘솔 출력 양식 따르기 여부
	 */
	public static printInWebkit:boolean = false;

	private static destFile:FS.WriteStream = null;
	private static destPathPrefix:string;
	private static destPathSuffix:string;
	private static _prevDestSwitched:number = NaN;
	private static destSwitchInterval:number;

	private static print(type:LogType, data:string):void{
		let now:Date = new Date();
		let message:string = data;
		let typeText:string;
		let clusterInfo:string = "";

		switch(type){
			case LogType.LOG: typeText = "%BRIGHT%LOG%NORMAL%"; break;
			case LogType.INFO: typeText = "%B_BLUE%INF%NORMAL%"; break;
			case LogType.SUCCESS: typeText = "%B_GREEN%%F_BLACK%OK!%NORMAL%"; break;
			case LogType.WARN: typeText = "%B_YELLOW%%F_BLACK%WAR%NORMAL%"; break;
			case LogType.ERROR: typeText = "%B_RED%%BRIGHT%ERR%NORMAL%"; break;
			default: throw Error("Unhandled log type: " + type);
		}
		if(Cluster.isMaster) clusterInfo = `%BRIGHT%%F_MAGENTA%#${process.pid}%NORMAL%`;
		else clusterInfo = `%F_MAGENTA%#${process.pid}%NORMAL%`;
		message = `%BRIGHT%[${now.toLocaleString()}]%NORMAL%${clusterInfo} ${typeText} ${message}`;

		if(Cluster.isMaster) Logger.printPure(message, now);
		else process.send({ type: "log", data: message, date: now });
	}
	private static printPure(message:string, now:Date):void{
		let time:number, timeR:number;

		if(Logger.printInWebkit){
			TinyLogger.printWebkit(message);
		}else{
			console.log(message.replace(TinyLogger.ESCAPE_MATCHER, (v, p1) => {
				if(LogColor.hasOwnProperty(p1)) return `\x1b[${LogColor[p1]}m`;
				return v;
			}));
		}
		timeR = (Logger.destSwitchInterval > 0)
			? (now.getTime() - now.getTimezoneOffset() * 60000) % Logger.destSwitchInterval
			: Infinity;
		time = now.getTime() - timeR;
		if(time !== Logger._prevDestSwitched && !isNaN(Logger._prevDestSwitched)){
			Logger.setDestFile(Format.ofDate(new Date(time)));
		}
		if(Logger.destFile !== null){
			Logger.destFile.write(message.replace(TinyLogger.ESCAPE_MATCHER, "") + "\n");
		}
		Logger._prevDestSwitched = time;
	}
	private static prettyListing(array:any[]):string{
		let factories:((v:string, i:number) => string)[];
		let length = array.length;

		if(length > 1){
			factories = [ (v, i) => v ];
			factories[length - 1] = (v, i) => `%BRIGHT%└ #${i}:%NORMAL% ${v}`;
			return array.map((v, i) => {
				return factories.hasOwnProperty(String(i)) ? factories[i](v, i) : `%BRIGHT%├ #${i}:%NORMAL% ${v}`;
			}).join('\n');
		}
		return String(array[0]);
	}
	private static setDestFile(name:string):void{
		if(Logger.destFile !== null) Logger.destFile.close();
		Logger.destFile = FS.createWriteStream(Logger.destPathPrefix
			+ name.replace(/\s/g, "_").replace(/[^\d_]/g, "")
			+ Logger.destPathSuffix);
	}

	/**
	 * 로그 파일 이름의 접사를 설정한다.
	 * 
	 * 로그 파일이 생성될 때의 시각에 접두사(prefix)와 접미사(suffix)를 덧붙여 이름을 결정한다.
	 * @param pathPrefix 로그 파일 이름의 접두사
	 * @param pathSuffix 로그 파일 이름의 접미사. 확장자를 포함한다.
	 */
	public static setDestFileAffix(pathPrefix:string, pathSuffix:string = ".log"):void{
		Logger.destPathPrefix = pathPrefix;
		Logger.destPathSuffix = pathSuffix;
		Logger.setDestFile(Format.ofDate(new Date()));

		Logger.info("Changed affix of log destination file", pathPrefix, pathSuffix);
	}
	/**
	 * 로그 파일 경로를 바꾸는 간격을 설정한다.
	 * 
	 * 하루마다 로그 파일 경로를 바꾸려면 인자로 `86400000`을 전달한다.
	 * @param interval 변경 간격(밀리초). 0인 경우 로그 파일 경로를 변경하지 않는다.
	 */
	public static setDestSwitchInterval(interval:number):void{
		Logger.destSwitchInterval = interval;

		if(interval > 0) Logger.info("Changed interval of switching log destination file", interval);
		else Logger.info("Disabled switching log destination file");
	}

	/**
	 * 별도 처리를 하지 않고 인자의 내용을 그대로 출력한다.
	 * 
	 * @param message 메시지
	 * @param timestamp 타임스탬프
	 */
	public static pure(message:string, timestamp:Date = new Date()):void{
		Logger.printPure(message, timestamp);
	}
	/**
	 * 일반 로그를 출력한다.
	 * 
	 * @param args 추가 정보
	 */
	public static log(...args:any[]):void{
		Logger.print(LogType.LOG, Logger.prettyListing(args));
	}
	/**
	 * 안내 로그를 출력한다.
	 * 
	 * @param args 추가 정보
	 */
	public static info(...args:any[]):void{
		Logger.print(LogType.INFO, Logger.prettyListing(args));
	}
	/**
	 * 성공 로그를 출력한다.
	 * 
	 * @param args 추가 정보
	 */
	public static success(...args:any[]):void{
		Logger.print(LogType.SUCCESS, Logger.prettyListing(args));
	}
	/**
	 * 경고 로그를 출력한다.
	 * 
	 * @param args 추가 정보
	 */
	public static warn(...args:any[]):void{
		Logger.print(LogType.WARN, Logger.prettyListing(args));
	}
	/**
	 * 오류 로그를 출력한다.
	 * 
	 * 이 메소드가 호출된다 해도 작업이 중단되지는 않는다.
	 * @param head 오류 유형
	 * @param args 추가 정보
	 */
	public static error(head:string, ...args:any[]):void{
		let message:string;

		Logger.print(LogType.ERROR, message = Logger.prettyListing([
			`%BRIGHT%${head}%NORMAL%`,
			...args
		]));
		// throw Error(message);
	}
}
enum LogType{
	LOG, INFO, SUCCESS, WARN, ERROR
}
enum LogColor{
	NORMAL = 0,
	BRIGHT,
	DIM,
	UNDERSCORE = 4,

	F_BLACK = 30,
	F_RED,
	F_GREEN,
	F_YELLOW,
	F_BLUE,
	F_MAGENTA,
	F_CYAN,
	F_WHITE,

	B_BLACK = 40,
	B_RED,
	B_GREEN,
	B_YELLOW,
	B_BLUE,
	B_MAGENTA,
	B_CYAN,
	B_WHITE
}